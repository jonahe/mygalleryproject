package com.jonahe.mygallery.controllers;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jonahe.mygallery.model.UserDTO;
import com.jonahe.mygallery.services.UserDatabaseService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet({ "/LoginServlet"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDatabaseService loginService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	System.out.println("Running LoginServlet#init()");

    	System.out.println("File.separator = " + File.separator);
    	String dbFolderPath = getServletContext().getRealPath("db");
    	System.out.println("getServletContext().getRealPath('db') = " + dbFolderPath);
    	
    	String dbFileName = getServletContext().getInitParameter("db-name");
    	loginService = new UserDatabaseService("jdbc:sqlite:" + dbFolderPath + File.separator + dbFileName);
    };

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String email, password;
		email = request.getParameter("email");
		password = request.getParameter("password");
		
		UserDTO user = loginService.authenticateAndGetUser(email, password);
		
		
		if(user == null) {
			System.out.println("auth = false");
			RequestDispatcher dispatcher = request.getRequestDispatcher("login");
			request.setAttribute("failed", "failed");
			dispatcher.forward(request, response);
			return;
		} else {
			System.out.println("auth = true");
			request.getSession().setAttribute("user", user);
			response.sendRedirect("pictures");
			return;
		}

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// GET-request is "not allowed", send user to login page
		// this path is used as a way to log out.
		request.getSession().setAttribute("user", null); // log out
		request.setAttribute("logout", "logout"); // used to show message to user
		RequestDispatcher dispatcher = request.getRequestDispatcher("login");
		dispatcher.forward(request, response);
	}
	
	
	
	

}
