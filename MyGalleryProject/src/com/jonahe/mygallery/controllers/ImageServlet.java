package com.jonahe.mygallery.controllers;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jonahe.mygallery.services.ImageDatabaseService;
import com.jonahe.mygallery.services.UserDatabaseService;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet(
		description = "Provides images (and their info) by user id, all public, or by search query", 
		urlPatterns = {
				"/ImageServlet/get-by-user",
				"/ImageServlet/get-by-needle"
				
		})

public class ImageServlet extends HttpServlet {
	private ImageDatabaseService imageDbService;
	private final String GET_BY_USER = "/ImageServlet/get-by-user";
	private final String GET_BY_NEEDLE = "/ImageServlet/get-by-needle";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init
	 */
	public void init() throws ServletException {
    	String dbFolderPath = getServletContext().getRealPath("db");
    	String dbFileName = getServletContext().getInitParameter("db-name");
    	imageDbService = new ImageDatabaseService("jdbc:sqlite:" + dbFolderPath + File.separator + dbFileName);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// set response type to json
		response.setContentType("application/json");
		
		// see what path was used
		String entryPath = request.getServletPath();
		
		if(entryPath.equals(GET_BY_USER)) {
			// getImageByOwnerId(request, response);
		} else if (entryPath.equals(GET_BY_NEEDLE)) {
			getImageByOwnerNeedle(request, response);	
		} else {
			System.out.println("Error: Unknown url pattern used?");
			return;
		}

	}

	private void getImageByOwnerNeedle(HttpServletRequest request, HttpServletResponse response) throws IOException {
		System.out.println("needle entry");
		// NOTE: if ownerId is less than 0, this means we want to include all users
		int ownerId, offset, limit;
		
		ownerId = Integer.parseInt(request.getParameter("userId"));
		offset = Integer.parseInt(request.getParameter("offset"));
		limit = Integer.parseInt(request.getParameter("limit"));
		
		String needle = request.getParameter("needle" );
		
		String resultJSONasText = imageDbService.getImagesByNeedle(needle, ownerId, offset, limit);
		if(resultJSONasText != null) {
			response.getWriter().append(resultJSONasText);
		} else {
			response.setContentType("text/html");
			response.getWriter().append("Served at: ").append(request.getContextPath() + "\n" + "nothing found!");
		}
		
	}

	/**
	 * @throws IOException 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	
//	private void getImageByOwnerId(HttpServletRequest request, HttpServletResponse response) throws IOException {
//		int ownerId = Integer.parseInt(request.getParameter("userId"));
//		System.out.println("UserId = " + ownerId);
//		
//		//TODO: fix so that "search all" also works here. (
//		
//		String resultJSONasText = imageDbService.getImagesByUserId(ownerId);
//		if(resultJSONasText != null) {
//			response.getWriter().append(resultJSONasText);
//		} else {
//			response.setContentType("text/html");
//			response.getWriter().append("Served at: ").append(request.getContextPath() + "\n" + "nothing found!");
//		}
//	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
