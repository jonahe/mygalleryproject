package com.jonahe.mygallery.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.jonahe.mygallery.helperclasses.ResultSetToJSONArrayConverter;

public class ImageDatabaseService {
	
	private final String dbFile;

	public ImageDatabaseService(String dbFile) {
		this.dbFile = dbFile;
		System.out.println("in ImageDatabaseService, full db path: " + dbFile);
	}
	
	
	/**
	 * Note return format
	 * @param userId
	 * @return result as json formatted String 
	 */
	public String getImagesByUserId(int userId) {
		
		String jsonAsText = null;
		Connection connection = getOpenConnection();
		
		try {
			Statement statement = connection.createStatement();
			
			statement.setQueryTimeout(3);  // set timeout to 10 sec.
			
			// createUserTableIfNotExists(statement);
			String query = "SELECT title, description, src, name FROM image JOIN user ON (image.owner = user.id) WHERE owner = " + userId;
			String searchAll = userId < 0 ? " OR 1" : "";
			query += searchAll; // or 1  --> always true, so this will get all imgs for all users
			System.out.println("Query: " + query);
			ResultSet rs = statement.executeQuery(query);
//			while(rs.next()){
//				//TODO: print stuff
//			}
			
			jsonAsText = ResultSetToJSONArrayConverter.convert(rs).toString();
			
			
			rs.close();
			
		} catch (SQLException e) {
			System.out.println("Db error. Could not create statement?");
			e.printStackTrace();
		}
		
		return jsonAsText;	
	}
	
	
	/**
	 * Note return format
	 * @param userId
	 * @return result as json formatted String 
	 */
	public String getImagesByNeedle(String needle, int userId, int offset, int limit) {
		
		String jsonAsText = null;
		Connection connection = getOpenConnection();
		
		try {
			Statement statement = connection.createStatement();
			
			statement.setQueryTimeout(3);  // set timeout to 10 sec.
			
			// createUserTableIfNotExists(statement);
			
			// NOTE: if ownerId is less than 0, this means we want to include all users
			String query = "SELECT title, description, src, name FROM image JOIN user ON (image.owner = user.id) WHERE (owner = ";
			if(userId < 0) {
				query += "-666 OR 1)"; //  always returns true
			} else {
				query += userId +")";
			}
			
			query += " AND (description LIKE '%" + needle + "%' OR (title LIKE '%" + needle + "%') " + "OR (name LIKE '%" + needle + "%'))";
			
			query += " LIMIT " + limit + " OFFSET " + offset;
			
			System.out.println("Query = " + query);
			ResultSet rs = statement.executeQuery(query);
//			while(rs.next()){
//				//TODO: print stuff
//			}
			
			jsonAsText = ResultSetToJSONArrayConverter.convert(rs).toString();
			
			rs.close();
			
		} catch (SQLException e) {
			System.out.println("Db error. Could not create statement?");
			e.printStackTrace();
		}
		
		return jsonAsText;	
	}
	
	
	
	
	
	private Connection getOpenConnection() {
		Connection connection = null;
		try {
			System.out.println("Creating db connection.");
			// load the sqlite-JDBC driver using the current class loader
		    Class.forName("org.sqlite.JDBC");
		    
			connection = DriverManager.getConnection(dbFile);
			
			
		} catch (SQLException e) {
			System.out.println("Could not find db file?");
			e.printStackTrace();
		}  catch (ClassNotFoundException e) {
			System.out.println("Could not find jdbc driver/class?");
			e.printStackTrace();
		}
		
		return connection;
	}
	
	

}
