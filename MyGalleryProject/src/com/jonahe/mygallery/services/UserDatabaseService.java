package com.jonahe.mygallery.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.jonahe.mygallery.model.UserDTO;


public class UserDatabaseService {

	private final String dbFile;
	private static Connection instance;

	/**
	 * 
	 * @param dbFile eg. "jdbc:sqlite:your_db_file.db"
	 */
	public UserDatabaseService(String dbFile) {
		this.dbFile = dbFile;
		System.out.println("Db-file path: " + dbFile);
	}
	
	public Connection getOpenConnectionInstance() {
//		if(instance != null) 
//			{ 
//			return instance;
//			}
//		else {
			try {
				System.out.println("in getOpenConnectionInstance() .");
				// load the sqlite-JDBC driver using the current class loader
			    Class.forName("org.sqlite.JDBC");
//			    Class classs = Class.forName("org.sqlite.JDBC");
//			    System.out.println("Class.forName('org.sqlite.JDBC') generated a class file of: " + classs.getClass().getName());
//			    
				instance = DriverManager.getConnection(dbFile);
				
				
			} catch (SQLException e) {
				System.out.println("Could not find db file?");
				e.printStackTrace();
			}  catch (ClassNotFoundException e) {
				System.out.println("Could not find jdbc driver/class?");
				e.printStackTrace();
			} catch (Exception e) {
				System.out.println("Unknown error. Could not find jdbc driver/class?");
				e.printStackTrace();
			}
			
			return instance;
		}
//	}
	
	/**
	 * 
	 * @param email
	 * @param password
	 * @return UserDTO with user details, or null if authentication fails.
	 */
	public UserDTO authenticateAndGetUser(String email, String password) {
		System.out.println("authenticating email: '" + email + "', password: '" + password + "'");
		UserDTO user = null;
		Statement statement = null;
		Connection connection = null;
		try {
			connection = getOpenConnectionInstance();
			System.out.println("After getOpenConnection.. connection = " + connection);
			statement = connection.createStatement();
			System.out.println("After createStatement().. statment = " + statement);
			statement.setQueryTimeout(10000);  // set timeout to 10 sec.
			
			
			// createUserTableIfNotExists(statement);
			
			ResultSet rs = statement.executeQuery("SELECT * FROM user WHERE email = '" + email + "' AND password = '" +password + "'");
			if(rs.next()){
				user = new UserDTO(rs);
			}
			
			rs.close();
			
			
			
		} catch (SQLException e) {
			System.out.println("authentication failed. db error?");
			e.printStackTrace();
		} finally {
			try {
				System.out.println("In finally block. connection = " + connection);
				if(connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("catch in finally block: " + e.getMessage());
				e.printStackTrace();
			}
		}
	
		
		return user;
	}
	
	public void closeDbConnection() {
		if(instance != null) {
			try {
				instance.close();
			} catch (SQLException e) {
				System.out.println("Could not close db connection.");
				e.printStackTrace();
			}
		}
	}
	
	
	private void createUserTableIfNotExists(Statement statement) throws SQLException {
	      System.out.println("Creating user table");
	      String createUserTable = 
	    		 "CREATE TABLE IF NOT EXISTS user" +
                "(id INT PRIMARY KEY     NOT NULL," +
                "email           TEXT    NOT NULL, " + 
                "name           TEXT    NOT NULL, " +
                "password        TEXT    NOT NULL) ";
	      
	      statement.executeUpdate("DROP TABLE IF EXISTS user");
	      statement.executeUpdate(createUserTable);
	      statement.executeUpdate("insert into user values(1, 'admin@admin.admin', 'Admin Adminsson', 'password')");
	      statement.executeUpdate("insert into user values(2, 'john@doe.anon', 'John Doe', 'password')");
	      statement.executeUpdate("insert into user values(3, 'erik@erik.erik', 'Erik Eriksson', 'password')");
	       ResultSet rs = statement.executeQuery("select * from user");
	      
	      
	      while(rs.next())
	      {
	        // read the result set
	    	System.out.println("id = " + rs.getInt("id"));
	    	System.out.println("email = " + rs.getString("email"));
	        System.out.println("name = " + rs.getString("name"));
	        System.out.println("password = " + rs.getString("password"));
	      }
	      
	      
	      rs.close();
	}
	
}
