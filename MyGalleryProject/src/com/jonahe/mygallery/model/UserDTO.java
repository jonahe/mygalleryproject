package com.jonahe.mygallery.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDTO {

	private int id;
	private String email;
	private String name;
	
	private static UserDTO guestUser;
	
	public UserDTO(ResultSet resultSet) {
		try {
			id = resultSet.getInt("id");
			email = resultSet.getString("email");
			name = resultSet.getString("name");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", email=" + email + ", name=" + name + "]";
	}
	
	public static UserDTO getGuestUser() {
		if(guestUser == null) {
			guestUser = new UserDTO(666, "guest@guest.guest", "Guest Guestsson");
		}
		return guestUser;
	}
	
	private UserDTO(int id, String email, String name) {
		this.id = id;
		this.email = email;
		this.name = name;
	}
	
	
	
	
	
}
