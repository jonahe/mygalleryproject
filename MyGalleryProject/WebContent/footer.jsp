
<div class="footer text-center">
	<div class="container-fluid">
	  <a id="to-top-arrow" href="#jumbo" title="Scoll to Top">
    	<span class="glyphicon glyphicon-chevron-up"></span>
  	  </a>
	
	<div class="row">
	
		<div class="col-sm-4">
			<h3>Tech-hype</h3>
			<p>JSP, Bootstrap, jQuery/JavaScript, AJAX, CSS, HTML, SQLite, Git</p>
		</div>
		
		<div id="login-info" class="col-sm-4">
			<h3>Login info</h3>
			<p>Users: erik@erik.erik, admin@admin.admin, john@doe.anon<br>Password: 'password' (without quotes)</p>
		</div>
		
		<div class="col-sm-4">
			<h3>Contact</h3>
			<p>Email: nope@nope.anon<br></p>
		</div>
		
	</div>  <!-- end of row -->
   </div>
	
	  <!-- Modal for logout confirmation -->
  <div class="modal fade" id="logout-modal" role="dialog">
   
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	      
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h2 class="modal-title">Really leave?</h2>
	        </div>
	        
	        <div class="modal-body">
	        
	         <div class="row">
		          <div class="col-sm-4">
		            <a href="LoginServlet" class="btn btn-default goodbye-option">Yes, I'm sure.</a>
		          </div>
		          
		          <div class="col-sm-4">
		          	<img id="goodbye-img" src="img/goodbye.jpg" class="img-circle img-responsive">
		          </div>
		          
		          <div class="col-sm-4">
		          	<button class="btn btn-default goodbye-option" data-dismiss="modal">No, not yet..</button>
		          </div>    
	          </div>
	        </div>
	
	      </div> 
	      
	    </div>
  </div> <!-- END Modal-->
  

</div>
</body>
</html>