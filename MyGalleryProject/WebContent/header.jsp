<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.jonahe.mygallery.model.UserDTO" %>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- BOOTSTRAP START -->

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap.css">

<!-- jQuery must be loaded first -->

<script src="js/jquery.js"></script>


<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="js/bootstrap.js"></script>


<!-- BOOTSTRAP END -->

<!--  custom scripts and styles -->

<link rel="icon" 
      type="image/png" 
      href="img/favicon.ico">


<!-- header and navbar js and style-->
<link rel="stylesheet" href="css/header.css">
<script src="js/header.js"></script>

<!-- main stylesheet.  -->
<link rel="stylesheet" href="css/main.css">

<!-- /pictures and style-->
<link rel="stylesheet" href="css/pictures.css">

<!-- footer style-->
<link rel="stylesheet" href="css/footer.css">
<script src="js/footer.js"></script>



<title>My gallery</title>
</head>





<%UserDTO user = (UserDTO) session.getAttribute("user");

  String loginVisibilityClass = "";
  String logoutVisibilityClass = "hidden";
  // if logged in..
  if(user != null) {
	  loginVisibilityClass = "hidden";
	  logoutVisibilityClass = "";
  }
%>


<!-- start of body -->
<body>


<!-- Header / Navigation bar start -->

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a id="collapsed-navbar-brand" class="navbar-brand" href=".">My gallery</a>
    </div>
    
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href=".">Home</a></li>
        <li class='<%=logoutVisibilityClass%>'><a href="pictures">Pictures</a></li> 
        <li class='<%=logoutVisibilityClass%>'><a href="profile">My profile</a></li>
        <li class="disabled"><a id="testtest" href="." data-toggle="tooltip" data-placement="bottom" title="Not implemented">About</a></li>
        
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class='disabled <%=loginVisibilityClass%>'>
        	<a href="#" data-toggle="tooltip" data-placement="bottom" title="Not implemented"><span class="glyphicon glyphicon-user"></span> Sign Up</a>
        </li>
        <li class='<%=loginVisibilityClass%>'><a href="login"><span class="glyphicon glyphicon-log-in "></span> Login</a></li>
        <li class='<%=logoutVisibilityClass%>'><a href="#" data-toggle="modal" data-target="#logout-modal"><span class="glyphicon glyphicon-log-in"></span> Log out</a></li>
      </ul>
    </div>
  </div>
</nav>

<div id="jumbo" class="jumbotron text-center">
	<h1 id="jumbo-h1">My gallery</h1>
	<p id="jumbo-p">Where pictures go to die</p>
</div>
