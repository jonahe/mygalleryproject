<%@ page language="java" %>


<%@include file="header.jsp"%>

<%
String failed = (String) request.getAttribute("failed");
String logout = (String) request.getAttribute("logout");
%>
<!-- redirect to profile page if user is logged in -->
<%
  if(user != null) {
	 // redirect to profile page if user is logged in 
	  	response.sendRedirect("profile");
  }
%>

<div id="page-content-wrapper"> <!-- Page content wrapper END -->
<!--  validation script -->
<script src="js/formtest.js"></script>
<!--  / validation script -->

	<!-- login starts here -->
	<div class="container-fluid">
		<div class="panel panel-primary">
	
			<div class="panel-heading">
				<h3>Login</h3>
			</div>
			<div class="panel-body">
				<form id="login-form" role="form" method="POST" action="LoginServlet" class="form-inline">
					
					<div class="form-group">
						
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
							<input name="email" type="text" required placeholder="email" class="form-control" list="predefined-users">
							
							<!-- list of users -->
							<datalist id="predefined-users">
							  <option value="admin@admin.admin">
							  <option value="john@doe.anon">
							  <option value="erik@erik.erik">
							</datalist>
							
						</div>
						
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input name="password" type="password" required placeholder="password" class="form-control">
						</div>
						<div class="input-group">
							<input id="submit-btn" type="submit" class="btn btn-success" value="Login">
						</div>
					
					</div>
				</form>
	
			</div>
	
			<div class="panel-footer">
				<%
					if (failed != null) {
				%>
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<p>No user with that name, or wrong password.</p>
				</div>
				<%
					};
				%>
				
				<%
			    if (logout != null) {
				%>
				<div class="alert alert-info fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<p>You are now logged out!</p>
				</div>
				<%
					};
				%>
			</div>
		</div>
	</div>

</div> <!-- Page content wrapper END -->	
	
<%@include file="footer.jsp"%>	
