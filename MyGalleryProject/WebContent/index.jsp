<%@include file="header.jsp"%>

<!-- redirect to profile page if user is logged in -->
<%
  if(user != null) {
	 // redirect to profile page if user is logged in 
	  	response.sendRedirect("pictures");
  }
%>

<div class="container-fluid">
	<div id="page-content-wrapper">
		<div class="row text-center">
			<h2>My gallery<sup>�</sup> is only open for registered users</h2>
				<div>
			        <a class="large-link" href="login">I have an account. Take me to login!</a>
				</div>
				<div>
			        <a class="large-link" href="." class="disabled">I'm new here. Take me to registration! <span class="label label-warning">Not implemented</span></a>
				</div>
		</div>
	</div>

</div>
<%@include file="footer.jsp"%>	