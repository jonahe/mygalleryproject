<%@ page language="java" %>

<%@include file="header.jsp"%> <!-- tries to set the variable user, and loads nav menu -->

<%
  if(user == null) {
	 // if user is not logged in, they don't have access to "mypictures"
	  	response.sendRedirect("login");
  }
%>

<script src="js/loadpictures.js"></script>

	<!-- Hidden info, to access later from javascript  -->
	
	<input type="hidden" id="userId" value='${user.id}' />

<div id="page-content-wrapper"> <!-- Page content wrapper Start -->

	<!-- Search bar -->
	<div class="container-fluid">
	  <div id="search-row" class="row">
		<!-- <div class="container-fluid">   -->
		 <div id="search-input-group" class="input-group">
			
		    <input id="search-input" placeholder="search" type="text" class="form-control input-lg" list="search-suggestions" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Title, description, owner" >
		    <!-- list of search suggestions.. -->		
				<datalist id="search-suggestions">
					  <option value="butterfly">
					  <option value="lizard">
					  <option value="sheep">
					  <option value="erik eriksson">
					  <option value="admin adminsson">
					  <option value="john doe"> 
				</datalist>
		    
		    	 
		   <div class="input-group-btn">
		        <button type="button" class="btn btn-default btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="search-btn-text">My pictures</span> <span class="caret"></span></button>
		        <ul id="search-dropdown" class="dropdown-menu dropdown-menu-right">
		          <li><a href="#" id="do-not-search-all" class="include-all-option">My pictures</a></li>
		          <li><a href="#" id="search-all" class="include-all-option">All pictures</a></li>
		        </ul>
		    </div><!-- /btn-group -->
		  </div><!-- /input-group -->
		
		  <!-- <input type="text" placeholder="search" /><span class="glyphicon glyphicon-search"></span><label class="checkbox-inline"><input id="search-all" type="checkbox" value="">Include everyone's pictures</label> -->
		<!-- </div> --> 
	   </div>
	</div>	<!-- End of search bar row -->
	
	<!-- search hits + slideshow/carousel buttons -->
	<div class="container-fluid"> 
			<div id="img-result-well" class="well well-sm">
				<p>Showing <span id="search-hits-nr" class="badge"></span> images 
	<!-- 				<a id="viewCarousel" href="#">Start/refresh slideshow <span class="glyphicon glyphicon-play-circle logo-sm"></span></a>
					<a id="hideCarousel" href="#">Stop slideshow <span class="glyphicon glyphicon-stop logo-sm"></span></a> -->
					<button type="button" id="viewCarousel" class="btn btn-default">Start/refresh slideshow <span id="carousel-icon" class="glyphicon glyphicon-play-circle logo-sm"></span></button>
					<button type="button" id="hideCarousel" class="btn btn-default">Stop slideshow <span id="carousel-icon" class="glyphicon glyphicon-stop logo-sm"></span></button>
				</p>
			</div>
	 </div>
	 
	<!--  start of carousel (populated on button click and hidden by default) -->	
		
	<div id="myCarouselDiv" class="container-fluid"> 
	   <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3500">
		  <!-- Indicators -->
		  <ol class="carousel-indicators" id="carousel-indicator">
		  </ol>
		
		  <!-- Wrapper for slides -->
		  <div id="carousel-items-container" class="carousel-inner" role="listbox">
			<div class="item active">
		      <img src="img/2015-07-01_1435773312.jpg" alt="">
		      <div class="carousel-caption">
		        <h3>Chania</h3>
		        <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
		      </div>
		    </div>
		  </div>
	
	
		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</div>

<!--  end of carousel test -->

	<!-- pagination. pics/page selector -->
	<div class="container-fluid text-center">
		<p>Pictures per page</p>
		<div class="btn-group">
			  <button type="button" class="btn btn-primary pics-per-page-option">10</button>
 			  <button type="button" class="btn btn-primary pics-per-page-option">15</button>
 			  <button type="button" class="btn btn-primary pics-per-page-option">20</button>
 			  <button type="button" class="btn btn-primary pics-per-page-option">50</button>
		</div>
<!-- 	    <div class="input-group">
	      <p>Showin </p> 
		   <div class="input-group-btn">
		        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="search-btn-text">My pictures</span> <span class="caret"></span></button>
		        <ul id="search-dropdown" class="dropdown-menu dropdown-menu-right">
		          <li><a href="#" class="pagination-option">My pictures</a></li>
		          <li><a href="#" class="pagination-option">All pictures</a></li>
		        </ul>
		    </div>/btn-group
		    <p>per page</p> 
		 </div>/input-group -->
	</div>

	<!-- pictures will be loaded here -->
	<div class="container-fluid">
		<div class="row" id="img-row">

	</div> <!-- End of pictures row  -->
		
	<div class="container text-center">                
	   <ul class="pagination">
	   		<li id="pag-nav-previous" class="pag-nav">
     			<a href="#" aria-label="Previous">
      		    <span aria-hidden="true">&laquo;</span>
     			</a>
	   		</li>
    		   		  
		    <li class="pagination-page-option active"><a href="#">1</a></li>
		    <li class="pagination-page-option"><a href="#">2</a></li>
		    <li class="pagination-page-option"><a href="#">3</a></li>
		    <li class="pagination-page-option"><a href="#">4</a></li>
		    <li class="pagination-page-option"><a href="#">5</a></li>
		    
		    <li id="pag-nav-next" class="pag-nav">
		      <a href="#" aria-label="Next">
		        <span aria-hidden="true">&raquo;</span>
		      </a>
		    </li>
		    
		    
	  </ul>
	</div>
	
	
	</div>


</div> <!-- Page content wrapper END -->	
	
<%@include file="footer.jsp"%>	
