
<%@include file="header.jsp"%> <!-- tries to set the variable user, and loads nav menu -->

<%
  if(user == null) {
	 // if user is not logged in, they don't have access to "mypictures"
	  	response.sendRedirect("login");
  }
%>

<div id="page-content-wrapper"> 	


	<div class="container-fluid text-center">
		<h2>Name: ${user.name}</h2>
		<h2>Email: ${user.email}</h2>
		<h2>User id: ${user.id}</h2>
		
		<div>
	        <a class="large-link" href="LoginServlet">Log out</a>
		</div>
		
	</div>
	
</div> <!-- Page content wrapper END -->

<%@include file="footer.jsp"%>