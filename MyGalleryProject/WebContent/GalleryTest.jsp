<%@ page language="java" %>
<%@ page import="java.io.File" %>
<%@ page import="java.lang.StringBuilder" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Gallery Test</title>
<!-- BOOTSTRAP START -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap.css">

<!-- jQuery must be loaded first -->

<script src="js/jquery.js"></script>

<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="js/bootstrap.js"></script>
</head>
<%!//private File dir = new File(getServletContext().getRealPath("img/animal_pics"));
// private File[] imgFiles = dir.listFiles();
// private File[] imgFiles = new File(getServletContext().getRealPath("img/animal_pics")).listFiles();

private String imgPath = "img/";
private String imgTagFormat = "<img alt='' src='%s' class='img img-thumbnail'>";

private String createImgTag(String src) {
	return String.format(imgTagFormat, src);
}

private String surroundWithColDiv(String imgTag) {
	return "<div class='col-sm-6 col-md-4'>" + imgTag + "</div>";
}

private String surroundWithRow(String column) {
	return "<div class='row'>" + column + "</div>";
}

private String createRow(String... imgTags){
	if(imgTags.length == 0) return "";
	
    StringBuilder sb = new  StringBuilder();
    for(String imgTag : imgTags) {
    	sb.append("<div class='col-md-4'>").append(imgTag).append("</div>");
    }
    
    return surroundWithRow(sb.toString());
}

private String getGalleryRows() {
    // getServletContext().getRealPath(arg0)
	//System.out.println(file.getPath());
	/* StringBuilder sb = new StringBuilder();
	for(File pic : imgFiles) {
		System.out.println(pic.getName());
	}
	 */
/* 	System.out.println(getServletContext().getRealPath("img/animal_pics"));
	 
	System.out.println(file.exists() + ", " + file.isDirectory() );
	
	System.out.println(imgPath + pics[0].getName()); */
	// String gallery = "";
	
	File file = new File(getServletContext().getRealPath("img/animal_pics"));
	File[] pics = file.listFiles();
	int count = 0;
	
	StringBuilder gallery = new StringBuilder();
	int rows = pics.length / 3;
	System.out.println("rows = " + rows);
	int picCount = 0;
	for(int i = 0; i < rows; i++) {
		if(picCount < pics.length) {
				
				for(int j = 0; j <= 2; j++) {
					if(picCount >= pics.length ) break; //TODO: tidy up... 
					
					if (count == 2) {
						gallery.append(surroundWithColDiv(createImgTag(imgPath + pics[picCount].getName())));

						gallery.append("</div>"); // end of row
						count = 0; // ready for new row
						picCount++;
						continue;
					} else {
						if (count == 0) {
							gallery.append("<div class='row'>"); // starting row
						}
						//TODO
						gallery.append(surroundWithColDiv(createImgTag(imgPath + pics[picCount].getName())));
						count++;
						picCount++;
					}
				}

			} else {
				break;
			}
		}
		

		// return createImgTag(imgPath + pics[0].getName());

		System.out.println("nr of pics should be " + pics.length);
		return gallery.toString();
	}


private String getGalleryCols() {

	File file = new File(getServletContext().getRealPath(imgPath));
	File[] pics = file.listFiles();
	int count = 0;

	StringBuilder gallery = new StringBuilder();
	    for(int i = 0; i < pics.length; i++) {
	    	gallery.append(surroundWithColDiv(createImgTag(imgPath + pics[i].getName())));
	    }
		

		// return createImgTag(imgPath + pics[0].getName());

		System.out.println("nr of pics should be " + pics.length);
		return gallery.toString();
	}




%>

<body>

	<div class="navbar navbar-inverse">
		<div class="container-fluid">
			<div>
				<ul class="nav navbar-nav">
					<li><a href="#">Nothing here</a></li>
					<li><a href="#">Nothing here</a></li>
					<li><a href="#">Nothing here</a></li>
					<li><a href="#">Nothing here</a></li>
				</ul>
			</div>		
		</div>
	</div>
	
	<div class="container-fluid">
<!-- 		<div class="row">
			<div class="col-md-4">
				<img alt="animal-01" src="img/animal_pics/2014-07-15_1405429868.jpg" class="img img-thumbnail">
			</div>
		</div> -->
		<%=getGalleryCols()%>
	
	</div>

</body>
</html>