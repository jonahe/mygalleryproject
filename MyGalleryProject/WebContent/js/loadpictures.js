
var fadeTimer = 0;
var $carouselIndicatorDiv;
var $carouselItemsContainer;
var lastGeneratedJsonArray;
var searchAll = false;
var searchNeedle = ""; // default
var loggedInUserId;


// PAGINATION VARIABLES
var pics_per_page = 50; // default
var current_pag_page = 1; // default
var total_nr_of_hits; 
var nr_of_pages;

function populateDivWithPictures($div, jsonArray) {
	$div.empty();
	for(var i = 0; i < jsonArray.length; i++) {
		$div.append(createImgDivFromJSON(jsonArray[i]));
		// change the fade effect -> illusion of loading in sequence.
		// this variable is used in createImgDivFromJSON method.
		if(i == jsonArray.length -1) {
			fadeTimer = 0;
		} else {
			if(fadeTimer < 2000) {
				fadeTimer += 50;				
			}
		}
	}
}

function populateCarousel(jsonArray){
	
	$('#myCarouselDiv').slideDown(500);
	
	$carouselIndicatorDiv.empty();
	$carouselItemsContainer.empty();
	
	for(var i = 0; i < jsonArray.length; i++) {
		$cIndicatorItem = createCarouselIndicator(i);
		$carouselIndicatorDiv.append($cIndicatorItem);
		
		$cItem = createCarouselItem(jsonArray[i], i);
		$carouselItemsContainer.append($cItem);
	}
	
}

function createCarouselIndicator(number) {
	return $('<li data-target="#myCarousel" data-slide-to="' + number + '"></li>');
}

function createCarouselItem(imgJson, number) {

	var active = (number != 0) ? "" : ' active';
	
	$item = $('<div class="item' + active + '"></div>');
	
	$img = $('<img>');
	$img.attr('src', "img/" + imgJson.src);
	$img.attr('alt', imgJson.title);
	
	$caption = $('<div class="carousel-caption"></div>');
	$caption.append($('<h3>' + imgJson.title + '</h3>'));
	$caption.append($('<p>' + imgJson.description + '</p>'));
	
	$item.append($img);
	$item.append($caption);
	
	return $item;
}




function getImgsAndPopulate() {
	
	var userId = loggedInUserId;
	var needle = searchNeedle;
	
	if(searchAll){
		userId = -666; // in ImageDatabaseService.java  this means all user ids will be ok, ie: search all
	}
	
	var offset = getOffset();
	console.log("pics per page is " + pics_per_page);
	var limit = pics_per_page;
	
	var path = "ImageServlet/get-by-user";
	// TODO: clear up: only use one path
	if(true) {  //  if( needle != "")
		path = "ImageServlet/get-by-needle";
	}
	// get all jsonarray(string) of all pictures from user
	$.get( path, {'userId' : userId, 'needle' : needle, 'offset' : offset, 'limit' : limit},
		function(resp) { 
		
			// the response is already parsed as JSON, in servlet: response.setContentType("application/json");
			lastGeneratedJsonArray = resp; // save for later use (slideshow creation)
			// show how many "hits"
			$('#search-hits-nr').html(resp.length);
		
			populateDivWithPictures( $('#img-row'), resp);

			}).fail( function() {
				alert("Ajax request failed. Can't load pictures");
			});
}


function createImgDivFromJSON(json) {
	// alert('in createImgDivFromJSON');
	$wrapper_div = $('<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"></div>');
	
	$imgWrapper = $('<div class="img-wrapper"></div>');
	
	$img = $('<img class="img img-thumbnail">');
	$title = $('<p class="img-title"></p>');
	
	// 
	$img.attr("src", "img/" + json.src);
	$img.attr("alt", json.title);
	$img.attr("desc", json.description);
	$title.html(json.title); // name = picture owner name
	$title.append($('<span class="picOwner">' + '' + json.name + '</span>'));
	
	// add a fade in to each image
	var fade = fadeTimer;
	$img.load( function () {
		$(this).hide();
		$(this).fadeTo(fade, 1);
	});
	
	$imgWrapper.append($img);
	$imgWrapper.append($title);
	
	$wrapper_div.append($imgWrapper);
//	$wrapper_div.append($img);
//	$wrapper_div.append($title);
	return $wrapper_div;
}



$(document).ready( function () {
	
	
	// pre-select search input field - but wait a while.. too much happening at once 
	setTimeout( function () {$('#search-input').focus()}, 1500);
	
	//fade in page content wrapper (everything execpt header/nav   and footer 
	$('#page-content-wrapper').slideUp(0);
	$('#page-content-wrapper').slideDown(1500);
	
	
	// set active picture per page buttons
	$('.pics-per-page-option:contains("' + pics_per_page + '")').first().addClass("active");
	
	
	// populate the first time
	loggedInUserId  = $('#userId').val();
	getImgsAndPopulate();
	
	// build pagination
	startPaginationBuildingProcess();
	
	// populate when user searches for something
	$('#search-input').on('input', function () {
		searchNeedle = $(this).val();
		// update search
		getImgsAndPopulate();
		// build pagination
		startPaginationBuildingProcess();
		
	});
	
	// prepare carousel
	$('#myCarouselDiv').hide();
	$('#hideCarousel').hide();
	
	$carouselIndicatorDiv = $('#carousel-indicator');
	$carouselItemsContainer = $('#carousel-items-container');
	
	$('#viewCarousel').click( function() {
		// show the stop/hide-button
		$('#hideCarousel').show();
		populateCarousel(lastGeneratedJsonArray);
	});
	
	
	$('.include-all-option').on('click' , function() {
		var id = $(this).attr('id');
		// check which option was selected
		if(id == 'do-not-search-all'){
			searchAll = false;
		} else if (id == 'search-all'){
			searchAll = true;
		} else {
			alert('unknown search option');
		}
		// change text of the button to show the selected option
		$('#search-btn-text').html($(this).html());
		
		// update result
		getImgsAndPopulate();
		
		// build pagination
		startPaginationBuildingProcess();
		
	});
	

	
	$('#hideCarousel').on('click', function () {
		$(this).hide();
		$('#myCarouselDiv').slideUp(500);
	});
	

	
// fade in title text on img hover, for dynamically created elements
	
	$("#img-row").on('mouseover', '.img-wrapper', 
		function () {
		

		// failed attempt at animating from mouse entry point.
//		var $elemHoveredOver = $(this);
//		var mouseEntryX; // relative to img
//		var mouseEntryY;
//	    $(document).mousemove(function(event){
	    	
//	    	var offset = $elemHoveredOver.offset();
//	    	
//	    	var mouseEntryX = event.pageX - offset.left;
//	    	var mouseEntryY = event.pageY - (offset.top + $elemHoveredOver.height());
//	    	
//	    	console.log("mouse over img: x" + mouseEntryX + ", y" + mouseEntryY);
//	    	
//	    });
		
		
		//  letters sliding from below/sides
		// set initial height
	    
	    $imgTitleElem =  $(this).find(".img-title");

	    $imgTitleElem.css( "bottom", "-5px");

		// initial spacing
	    $imgTitleElem.css( "letter-spacing", "10px");
		
		// animate
	    $imgTitleElem.stop(false, true).animate( { opacity: 1, letterSpacing: '0px', bottom: '10px', left: '0px'}, 'slow');
		// $(this).find(".img-title").fadeIn(400);
		

	});
	
	// //  letters sliding away below/middle 
	$("#img-row").on('mouseout', '.img-wrapper', 
		function () {
		
		 $(this).find(".img-title").stop(false, true).animate( { opacity: 0, letterSpacing: '-3px', bottom: '0px'}, 'fast');

	});
	
	
	// hide info in carousel mode
	$('#carousel-items-container').on('click', '.item', function() {
		// alert("click");
		
		$('.carousel-caption').toggleClass("hidden");
		$('.carousel-indicators > li').toggleClass("hidden");
	});
	
	
// ------------------------------------------------------------------------------	
	
	// PAGINATION
	
	// - listnener for pics-per-page buttons
	$('.pics-per-page-option').click(function () {
		$('.pics-per-page-option').removeClass("active");
		$(this).addClass("active");
		
		// save the chosen option for access elsewhere
		pics_per_page = parseInt( $(this).html(), 10);
		
		// reset to page 1
		current_pag_page = 1;
		
		//update pictures
		getImgsAndPopulate();
		
		// build pagination
		createPagination();
	});
	
	// - listnener for current pagination page buttons, DYNAMICALLY CREATED
	
	$('.pagination').on('click', '.pagination-page-option', function () {
		
		event.preventDefault();
		
		$('.pagination-page-option').removeClass("active");
		$(this).addClass("active");
		
		// save the chosen option for access elsewhere
		var pageNr = parseInt( $(this).find("a").html(), 10);
		
		// only update if we're actually changing page
		if(pageNr != current_pag_page) {
			// update value
			current_pag_page = parseInt( $(this).find("a").html(), 10);
			
			// scroll to top
			$('#to-top-arrow').trigger('click');
			// THEN populate imgs
			setTimeout(  getImgsAndPopulate, 800); 
		}
	});
	
	// previous / next page listeners
	
	$('.pag-nav').click( function() {
		var id = $(this).attr('id');
		
		// go back
		if(id == 'pag-nav-previous') {
			
			if(current_pag_page != 1) {
				current_pag_page -= 1;
			} else {
				event.preventDefault();
				return;
			}
		
		// go forward	
		} else if (id == 'pag-nav-next'){
			
			if(current_pag_page != nr_of_pages) {
				current_pag_page += 1;
			} else {
				event.preventDefault();
				return;
			}
		
		} else {
			alert("unknown pag-nav btn");
		}
		
		updateActivePagination();
		
		getImgsAndPopulate();
		
		
	});
	
	
	
});


// PAGINATION FUNCTIONS

function getOffset() {
	return pics_per_page * (current_pag_page - 1);
}


	//to be run after each search input change, or after "search-all" change. 
function startPaginationBuildingProcess() {
	getNrHitsWithoutLimitOrOffset();
}

function getNrHitsWithoutLimitOrOffset() {
	var userId = loggedInUserId;
	
	if(searchAll){
		userId = -666; // in ImageDatabaseService.java  this means all user ids will be ok, ie: search all
	}
	
	// get query results
	new function () { 
			$.get( "ImageServlet/get-by-needle", {'userId' : userId, 'needle' : searchNeedle, 'offset' : 0, 'limit' : 1000},
	
					function(resp) { 
						return saveTotalNrOfHits(resp); // callback
					}).fail( function() {
						// alert("Ajax request failed. Can't count result rows");
						return "failed";
					});
		}();

}
	// callback for ajax call, also triggers pagination building
function saveTotalNrOfHits(resp) {
	total_nr_of_hits = resp.length;
	console.log("total nr of hits: " + total_nr_of_hits);
	
	createPagination();
}

// callback function, run after async call (or after page change)
function createPagination() {
	
	nr_of_pages = Math.ceil(total_nr_of_hits / pics_per_page);
	console.log("nr of pages needed: " + nr_of_pages);
	
	removePaginationOptions();
	
	for(var i = 1; i <= nr_of_pages; i++) {
		$pageOptItem = $('<li class="pagination-page-option"><a href="#">' + i + '</a></li>');
		if(i == current_pag_page) {
			$pageOptItem.addClass('active');
		}
		
		$($pageOptItem).insertBefore('#pag-nav-next');
	}
	
}

function removePaginationOptions() {
	$('.pagination').find('.pagination-page-option').remove();
}

function updateActivePagination() {
	$('.pagination-page-option').each( function() {
		$(this).removeClass('active');
		$aTag = $(this).find('a');
		if( $aTag.html() == current_pag_page) {
			$(this).addClass('active');
		}
	})
}

