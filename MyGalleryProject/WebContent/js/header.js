var heightWhereJumboDissapears = 125; //px
var $jumbotron;
var $jumbo_h1;
var $jumbo_p;
var initialJumbotronHeight;
var previousScrollVal;



$(document).ready( function() {
		
	initializeAndSaveStartingPoint();
	
	
	// disabled because it was buggy
	
	// on scroll
//	$(document).scroll(function() {
//		var yHideShowCutOff = 100;
//		var scrollYpos = $(this).scrollTop();
//		
//		// scrolling down
//		if(isScrollingDown(scrollYpos)) {
//			if($jumbotron.is(":visible") && scrollYpos < yHideShowCutOff) {
//				$('#search-row').addClass("addedTopPadding");
//				$jumbotron.slideUp("slow", "linear", function() { scrollToId("#search-input")});
//
// 				previousScrollVal = 0;
////				$(document).scrollTop(0);
//			}
//		// scrolling up	
//		} else {
//			if( (! $jumbotron.is(":visible")) && scrollYpos < (yHideShowCutOff - 20)) {
//				$('#search-row').removeClass("addedTopPadding");
//				$jumbotron.slideDown("slow");
//			}
//		}
//	}),
	
	// enable all tooltips
	$('[data-toggle="tooltip"]').tooltip();

	
});

function initializeAndSaveStartingPoint(){
	  $jumbotron = $('#jumbo');
	  $jumbo_h1 = $('#jumbo-h1');
	  $jumbo_p = $('#jumbo-h1');
	  
	  initialJumbotronHeight = parseInt($jumbotron.css('height'), 10);
	  previousScrollVal = $(document).scrollTop();
}

function isScrollingDown(currentScrollVal){
	return currentScrollVal > previousScrollVal;
}


function scrollToId(id){
//    var $aTag = $(id);
//    var scrollTo = $aTag.offset().top() - $aTag.height();
//    $('html,body').animate({scrollTop: 50},'slow');
}