/**
 * 
 */

$(document).ready( function () {
	
	animateActiveInput();
	
	// pre-select first input field - but wait a while.. too much happening at once 
	setTimeout( function () {$('input[name="email"').focus()}, 1500);
	
	$("#submit-btn").attr("disabled", "disabled");
	
	// $('#login-info').animate({backgroundColor: 'green'}, 'slow', function() {console.log("animation over")});
	$('#login-info').addClass('fadeToYellow');
	
	$("input").on("input", function () { // change keyup paste 
		if(checkPasswordValidity() && userNameNotEmpty()) {
			console.log("removing disable");
			$("#submit-btn").removeAttr("disabled");
		} else {
			console.log("adding disable");
			$("#submit-btn").attr("disabled", "disabled");
		}
	});
});

function checkPasswordValidity() {
	var pw = document.forms['login-form']['password'].value;
	
	var arr = pw.split(" ");
	console.log("password: " + arr);
	if(arr.length > 1) {
		// alert("password must not contain spaces!");
		// console.log(pw);
		return false;
	} else if (pw.length < 6) {
		// alert("password must be at least 6 characters long");
		return false;
	}
	
	return true;
	
}

function userNameNotEmpty() {
	var username = document.forms['login-form']['email'].value;
	return username.length != 0;
}

function animateActiveInput(){
	$('input').on("focus", function() {
		$(this).parent().find(".glyphicon").addClass("rotating-glyph");
		if($(this).attr("name") == "password") {
			$('#login-info').toggleClass('fadeToYellow');
			setTimeout( function () {$('#login-info').toggleClass('fadeToYellow')}, 200);
		}
	});
	
	$('input').on("focusout", function() {
		$(this).parent().find(".glyphicon").removeClass("rotating-glyph"); 
		// $(this).parent().find(".input-group-addon").animate({color: 'green'}); 
	});
	
}
